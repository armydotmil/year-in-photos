module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jslint: {
            client: {
                src: [
                    'src/_js/**/*.js'
                ],
                directives: {
                    browser: true
                },
                exclude: [
                    'src/_js/backbone-min.js',
                    'src/_js/bootstrap.min.js',
                    'src/_js/modernizr.custom.js',
                    'src/_js/owl.carousel.custom.min.js',
                    'src/_js/spin.min.js',
                    'src/_js/underscore-min.js',
                    'src/_js/fastclick.js',
                    'src/_js/waypoints.min.js'
                ],
                options: {

                }
            }
        },
        jshint: {
            all: [
                'src/_js/**/*.js'
            ],
            options: {
                ignores: [
                    'src/_js/backbone-min.js',
                    'src/_js/modernizr.custom.js',
                    'src/_js/spin.min.js',
                    'src/_js/underscore-min.js',
                    'src/_js/fastclick.js',
                    'src/_js/waypoints.min.js'
                ]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'src/to_origin/rv5_css/yearinphotos/2015/style.css': 'src/_scss/style.scss'
                }

            }
        },
        jsbeautifier: {
            files: ['src/**/*.html'],
            options: {}
        },
        accessibility: {
            options: {
                accessibilityLevel: 'WCAG2A'
            },
            test: {
                src: ['src/**/*.html']
            }
        },
        zip: {
            'build/rv5_images.zip': ['src/to_origin/rv5_images/**/*']
        },
        unzip: {
            highlight: {
                src: ['build/rv5_images.zip'],
                dest: '.'
            }
        },
        replace: {
             local: {
                src: ['src/**/*.html', 'src/_js/*.js', 'src/_scss/*.scss'],
                overwrite: true,
                replacements: [{
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
                    to: 'http://localhost:8282/to_origin/'
                }]
            },
            fixes: {
                src: ['src/**/*.html', 'src/_js/*.js', 'src/_scss/*.scss'],
                overwrite: true,
                replacements: [{
                    from: '<link rel="stylesheet" href="/e2/rv5_css/yearinphotos/2015/style.css"/> ',
                    to: '<link rel="stylesheet" href="/e2/rv5_css/3rdparty/bootstrap/bootstrap.min.css">'
                }]
            },
            cdn: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: 'http://localhost:8282/to_origin/',
                    to: 'http://www.army.mil/e2/'
                }, {
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
                    to: 'http://www.army.mil/e2/'
                }, {
                    from: 'http://frontend.ardev.us/api/',
                    to: 'http://www.army.mil/api/'
                }]
            },
             dev: {
                src: ['src/**/*.html', 'src/_js/*.js', 'src/_scss/*.scss'],
                overwrite: true,
                replacements: [{
                    from: 'http://localhost:8282/to_origin/',
                    to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
                }]
            }
        },
        'http-server': {
            'dev': {
                root: 'src/',
                port: 8282,
                host: "0.0.0.0",
                ext: "html",
                runInBackground: false
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                compress: false,
                beautify: false
            },
            build: {
                src: [
                    'src/_js/photos.js'
                ],
                dest: 'src/to_origin/rv5_js/yearinphotos/2015/photos.min.js'
            }
        },
        watch: {
            scripts: {
                files: ['src/_js/**/*.js', 'src/_scss/**/*.scss'],
                tasks: ['sass', 'uglify'],
                options: {
                    spawn: false,
                },
            },
        },
        'sftp-deploy': {
            build: {
                auth: {
                    host: 'frontend.ardev.us',
                    authKey: 'privateKey'
                },
                cache: 'sftpCache.json',
                src: 'src/',
                dest: '/www/development/<%= pkg.name %>',
                exclusions: ['build/', 'node_module/', 'Gruntfile.js', 'package.json', 'readme.md', '.sass-cache', '.git', '.gitignore', 'src/to_origin/rv5_images'],
                serverSep: '/',
                concurrency: 4,
                progress: true
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        }
    });

    grunt.loadNpmTasks('grunt-bump');

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('grunt-sftp-deploy');

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-http-server');

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.loadNpmTasks('grunt-curl');

    grunt.loadNpmTasks('grunt-zip');

    grunt.loadNpmTasks('grunt-accessibility');

    grunt.loadNpmTasks('grunt-jslint');

    grunt.loadNpmTasks("grunt-jsbeautifier");

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['images', 'local', 'http-server']);

    grunt.registerTask('dev', ['replace:dev', 'sass', 'uglify']);

    grunt.registerTask('local', ['replace:local', 'sass', 'uglify']);

    grunt.registerTask('cdn', ['replace:cdn', 'sass', 'uglify']);

    grunt.registerTask('fixes', ['replace:fixes', 'sass', 'uglify']);

    grunt.registerTask('images', ['curl', 'unzip']);

};
