//Last Updated: 2015-01-22 0852 JH 
var photo_data = [{
        january: [{
            file_name: "0101",
            date: "Jan. 1, 2015",
            by_line: "Photo by U.S. Army Spc. Rashene Mincy",
            caption: "Spc. Morgan Austin, communications specialist with Joint Forces Command – United Assistance, assigned to 101st Airborne Division (Air Assault), holds up the U.S. flag during a promotion and re-enlistment ceremony at Barclay Training Center, Monrovia, Liberia. United Assistance is a DOD operation in Liberia to provide logistics, training and engineering support to U.S. Agency for International Development-led efforts to contain the Ebola virus outbreak in western Africa.",
            alt: "2015 Year in Pictures : A United States Army Soldier from 101st Airborne Division holds an American flag in Liberia.",
            homepage: ""
        }, {
            file_name: "0103",
            date: "Jan. 3, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "Medal of Honor recipient retired Master Sgt. Leroy Petry walks onto the field of the Alamodome in San Antonio with World War II veteran Richard Overton in San Antonio. Petry, awarded the Medal of Honor last year for efforts in Afghanistan, and Overton, the oldest living World War II veteran at 108 years old, delivered the game ball at the U.S. Army All-American Bowl.",
            alt: "2015 Year in Photos : Medal of Honor recipient from the United States Army walks onto the field with World War II veteran before the All-American Bowl.",
            homepage: ""
        }, {
            file_name: "0113",
            date: "Jan. 13, 2015",
            by_line: "Photo by U.S. Army Visual Information Specialist Paolo Bovo",
            caption: "Paratroopers from 173rd Brigade Support Battalion, 173rd Airborne Brigade, conduct an airborne operation from a U.S. Air Force 86th Air Wing C-130 Hercules aircraft at Juliet Drop Zone in Pordenone, Italy. The 173rd Airborne Brigade is the Army Contingency Response Force in Europe, capable of projecting ready forces anywhere in the U.S. European, Africa or Central Command areas of responsibility within 18 hours.",
            alt: "United States Army paratroopers conduct airborne operations in Italy with parachutes and mountains in the backdrop.",
            homepage: ""
        }, {
            file_name: "0113_3",
            date: "Jan. 13, 2015",
            by_line: "Photo by U.S. Army Chief Warrant Officer 4 Mark Leung",
            caption: "Eighteen OH-58D(R) Kiowa Warriors from 2-6 Cavalry took to the sky above Oahu during a rehearsal for the unit’s symbolic final flight in Hawaii, which occurred, Jan. 15, prior to the 2-6 Cavalry color casing ceremony, in preparation for the Squadron's deployment to South Korea, in support of 2nd Combat Aviation Brigade. ",
            alt: "Two Kiowa Warrior helicopters flying in Hawaii in front of mountains in Oahu.",
            homepage: ""
        }, {
            file_name: "0121",
            date: "Jan. 21, 2015",
            by_line: "Photo by U.S. Army Spc. Benjaman Pollhein",
            caption: "A U.S. Army Soldier, participating in a rotation at Lighting Academy's Jungle Operations Training Center, crosses a river at the 25th Infantry Division East Range Training Complex in Hawaii.",
            alt: "A United States Army Soldier participates in jungle training by using a rope to cross a river in Hawaii.",
            homepage: ""
        }, {
            file_name: "0122",
            date: "Jan. 22, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Mikki L. Sprenkle",
            caption: "14th Sgt. Maj. of the Army Raymond F. Chandler III kisses his wife, Jeanne Chandler, at their farewell ceremony in the Pentagon, Arlington, Va.",
            alt: "A black and white photo of the 14th Sergeant Major of the Army kissing his wife at his farewell ceremony in the Pentagon.",
            homepage: ""
        }, {
            file_name: "0123",
            date: "Jan. 22, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Javier Orona",
            caption: "Paratroopers from 2nd Battalion, 501st Parachute Infantry Regiment, 1st Brigade Combat Team, 82nd Airborne Division, fire their weapons during reflexive fire training on Fort Bragg, N.C.",
            alt: "Paratroopers fire their weapons during reflexive fire training at the shooting range on Fort Bragg, N.C.",
            homepage: ""
        }, {
            file_name: "0123_2",
            date: "Jan. 23, 2015",
            by_line: "Photo by U.S. Army Spc. Caitlyn Byrne",
            caption: "Command Sgt. Maj. Gregory F. Nowak of Joint Forces Command – United Assistance and the 101st Airborne Division (Air Assault), right, and Command Sgt. Maj. Ian Griffin of 101st Sustainment Brigade, left, deployed as Task Force Lifeliner, inspect the undercarriage of a vehicle at the U.S.  Department of Agriculture inspection point on Camp Buchanan, Liberia. Soldiers operate the USDA inspection point to ensure that all vehicles and equipment headed back to the U.S. meet customs standards. ",
            alt: "United States Army Soldiers inspect underneath a vehicle at the U.S.  Department of Agriculture inspection point at Camp Buchanan, Liberia.",
            homepage: ""
        }, {
            file_name: "0128",
            date: "Jan. 28, 2015",
            by_line: "Photo by U.S. Army Spc. Rashene Mincy",
            caption: "U.S. Army Spc. Michael Saucier, a truck driver with the 101st Sustainment Brigade, Task Force Lifeliner, Joint Forces Command – United Assistance, pressure washes the bottom of a Light Medium Tactical Vehicle on Camp Buchanan, Liberia. Soldiers deployed in support of Operation United Assistance are required to wash military vehicles, preparing them for the arrival of a ship that will take the vehicles back to the U.S.",
            alt: "2015 Year in Pictures : A United States Army Soldier pressure washes the bottom of a Light Medium Tactical Vehicle, or LMTV at Buchanan, Liberia.",
            homepage: ""
        }, {
            file_name: "0130",
            date: "Jan. 30, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Chuck Burden",
            caption: "Sgt. Maj. Daniel A. Dailey thanks his mother during the swearing-in ceremony for Dailey as the 15th sergeant major of the Army at the Pentagon in Arlington, Va. Dailey will serve as the Army chief of staff’s personal adviser on matters affecting the Army’s enlisted force.",
            alt: "United States Sergeant Major of the Army Daniel A. Dailey thanks his mother during his swearing-in ceremony.",
            homepage: ""
        }],
        february: [{
            file_name: "0203",
            date: "Feb. 3, 2015",
            by_line: "Photo by U.S. Army Pfc. Samantha Van Winkle",
            caption: "U.S. Army Capt. Ryan Mortensen, a chaplain assigned to 1st Battalion, 27th Infantry Regiment, 2nd Stryker Brigade Combat Team, 25th Infantry Division, interacts with children of the Middle Mosque of Lop Buri, Thailand. Cobra Gold 15 allows U.S. Soldiers to bond with the Thai community, continuing a 182-year tradition of friendship. ",
            alt: "A United States Army Soldier from the Chaplain Corps giving a young Thai child candy.",
            homepage: ""
        }, {
            file_name: "0205",
            date: "Feb. 5, 2015",
            by_line: "Photo by U.S. Army Spc. Jess Nemec",
            caption: "A U.S. Army flight medic, assigned to 3rd Aviation Regiment, 3rd Infantry Division, prepares two 24th MEU Marines for hoisting into a UH-60 Black Hawk medical evacuation helicopter during a casualty evacuation exercise near Camp Buehring, Kuwait.",
            alt: "A United States Army flight medic prepares marines to be hoisted into a Black Hawk helicopter during a MEDEVAC exercise.",
            homepage: ""
        }, {
            file_name: "0211",
            date: "Feb. 11, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Mary S. Katzenberger",
            caption: "Paratroopers assigned to 3rd Brigade Combat Team, 82nd Airborne Division, descend toward Holland Drop Zone against the backdrop of a crystal clear sky and last quarter moon during an airborne proficiency jump on Fort Bragg, N.C.",
            alt: "Paratroopers with parachutes expanded, descending toward Holland Drop Zone against the backdrop of a crystal clear sky and last quarter moon.",
            homepage: ""
        }, {
            file_name: "0212",
            date: "Feb. 12, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Chuck Burden",
            caption: "U.S. Army Gen. John F. Campbell, commander of the U.S. forces in Afghanistan, speaks on the situation in Afghanistan during the Senate Armed Services Committee hearing in Washington, D.C.",
            alt: "General John F. Campbell, commander of the U.S. forces in Afghanistan, speaks on the situation in Afghanistan during a SASC hearing in Washington, D.C.",
            homepage: ""
        }, {
            file_name: "0219",
            date: "Feb. 17, 2015",
            by_line: "Photo by U.S. Army Visual Information Specialist Jason Johnston",
            caption: "U.S. Army Soldiers train with multinational soldiers at the International Special Training Center, Advanced Medical First Responder Course in Pfullendorf, Germany. The center provides centralized training for NATO Special Forces and similar units in Europe and is run by a cadre of instructors from nine member nations-Belgium, Denmark, Germany, Greece, Italy, the Netherlands, Norway, Turkey and the United States.",
            alt: "2015 Year in Pictures : A soldier carrying his battle buddy who is holding an M16 model A2, a military-grade assault rifle, in the snow.",
            homepage: ""
        }, {
            file_name: "0219_2",
            date: "Feb. 19, 2015",
            by_line: "Photo by U.S. Air National Guard Tech. Sgt. Sarah Mattison",
            caption: "Soldiers attending the U.S. Army Mountain Warfare School in Jericho, Vt., climb Smugglers' Notch as part of their final phase of the Basic Military Mountaineering Course in Jeffersonville, Vt. Students in the Basic Military Mountaineering course spend two weeks acquiring the skills and knowledge required to operate in mountainous terrain.",
            alt: "United States Army Soldiers hiking up a mountain in the snow in Vermont during the Basic Military Mountaineering Course.",
            homepage: ""
        }, {
            file_name: "0219_3",
            date: "Feb. 19, 2015",
            by_line: "Photo by U.S. Marine Corps Cpl. James Marchetti",
            caption: "U.S. Army Staff Sgt. Samnang Virakpanyou, a construction engineering supervisor with the Washington National Guard’s 176th Engineering Company (Vertical), participates in an invocation led by a Buddhist monk during a column raising ceremony at Ban Sub Prik Elementary School, located in Muak Lek District, Saraburi Province, Thailand. ",
            alt: "A United States Army soldier holds burning incents as part of a prayer led by a Buddhist monk",
            homepage: ""
        }, {
            file_name: "0220",
            date: "Feb. 20, 2015",
            by_line: "Photo courtesy of the U.S. Army Reserve 200th Military Police Command",
            caption: "A Soldier, assigned to the U.S. Army Reserve's 200th Military Police Command, rappels 600 feet down Rialto Towers, Melbourne, Australia, part of Tactical Working at Height course conducted with the Victoria Police Special Operations Group. The course is designed to impart skills for safe operation in the field of tactical roping and high-risk access for police and military operators. This is the first time the U.S. Army participated in the training.",
            alt: "A United States Army Reserve Soldier from the 200th MP rappels down a building as part of training.",
            homepage: ""
        }, {
            file_name: "0220_3",
            date: "Feb. 20, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Nicolas Morales",
            caption: "Despite minus 10 degree temperatures, Soldiers from The Old Guard's 1st Battalion and 4th Battalion perform their duties with precision while honoring our nation’s fallen in Arlington National Cemetery, Va.",
            alt: "2015 Year in Photos : The Old Guard performs a gun salute during a funeral ceremony at Arlington National Cemetery during below-freezing weather.",
            homepage: ""
        }, {
            file_name: "0224",
            date: "Feb. 24, 2015",
            by_line: "Photo by U.S. Army Visual Information Specialist Jason Johnston",
            caption: "A U.S. Army Soldier, assigned to 1st Battalion, 10th Special Forces Group (Airborne), salutes his fellow Soldiers while jumping out of a C-130 Hercules aircraft over a drop zone in Germany.",
            alt: "A United States Army Soldier salutes as he jumps out of a C-130 Hercules over Germany on February 24.",
            homepage: ""
        }],
        march: [{
            file_name: "0303",
            date: "March 3, 2015",
            by_line: "Photo by U.S. Air Force civilian photographer Justin Connaher",
            caption: "U.S. Army Pfc. Ryein Weber, assigned to Apache Company, 1st Battalion, 501st Infantry Regiment (Airborne), 4th Brigade Combat Team (Airborne), 25th Infantry Division, U.S. Army Alaska, qualifies with a M249 Squad Automatic Weapon on Grezelka range at Joint Base Elmendorf-Richardson, Alaska. ",
            alt: "A United States Army Private First Class Soldier shoots an M249 Squad Automatic Weapon at a firing range in Alaska.",
            homepage: ""
        }, {
            file_name: "0305",
            date: "March 5, 2015",
            by_line: "Photo by U.S. Army Sgt. William Tanner",
            caption: "Dragoons, assigned to Bull Troop, 1st Squadron, 2nd Cavalry Regiment, participate in a live-fire exercise at Grafenwoehr Training Area located near Rose Barracks, Germany. Troopers conducted group and tactical movements while attacking an objective assisted by their Strykers and mortar fire during the exercise.",
            alt: "United States Army Soldiers move tactically during a live-fire exercise in Germany with green smoke in the background.",
            homepage: ""
        }, {
            file_name: "0306",
            date: "March 6, 2015",
            by_line: "Photo by U.S. Army Sgt. Timothy Clegg",
            caption: "A U.S. Army Soldier, assigned to 10th Special Forces Group (Airborne), shows children how to perform proper pushups in Mao, Chad. The 10th SFG (A) took part in Exercise Flintlock 2015, an annual, African-led military exercise focused on security, counter terrorism and military humanitarian support to outlying areas.",
            alt: "United States Army show African children how they perform physical training, or PT, by doing push-ups.",
            homepage: ""
        }, {
            file_name: "0309",
            date: "March 9, 2015",
            by_line: "Photo by U.S. Marine Corps Sgt. Paul Peterson",
            caption: "A dog handler with U.S. Army Special Forces clears a building during a close-quarters battle demonstration for Marines with Special-Purpose Marine Air-Ground Task Force Crisis Response-Africa in Baumholder, Germany. The use of military dogs is an efficient way to detect hazardous materials and locate individuals within a structure.",
            alt: "A dog handler and his service dog with U.S. Army Special Forces clear a building during a close-quarters battle demonstration for Marines.",
            homepage: ""
        }, {
            file_name: "0316",
            date: "March 16, 2015",
            by_line: "Photo by U.S. Marine Corps Lance Cpl. Brittney Vella",
            caption: "A U.S. Army Soldier with Operational Detachment Alpha 1215, 1st Special Forces Group, Joint Base Lewis-McChord, runs off the back of a CH-47F Chinook helicopter while conducting a simulated combat dive mission in the water off of Marine Corps Training Area Bellows. The helicopter hovered the ocean and allowed the Soldiers to conduct a boat movement leading to reconnaissance of the beach and a raid in the training facility at Bellows.",
            alt: "A United States Army Soldier holds his swimming equipment as he jumps out of the back of a CH-47F Chinook helicopter.",
            homepage: ""
        }, {
            file_name: "0318_2",
            date: "March 18, 2015",
            by_line: "Photo by U.S. Air Force civilian photographer Justin Connaher",
            caption: "Paratroopers, assigned to the 4th Infantry Brigade Combat Team (Airborne), 25th Infantry Division, U.S. Army Alaska, practice a forced-entry parachute assault on Malemute drop zone at Joint Base Elmendorf-Richardson, Alaska as part of a larger tactical field exercise. The Soldiers are part of the Army’s only Pacific airborne brigade with the ability to rapidly deploy worldwide, and are trained to conduct military operations in austere conditions.",
            alt: "United States Army paratroopers practice a forced-entry parachute assault on Malemute drop zone. The Soldiers are part of the Army's Pacific airborne brigade.",
            homepage: ""
        }, {
            file_name: "0319_1",
            date: "March 19, 2015",
            by_line: "Photo by U.S. Army Capt. Charlie Emmons",
            caption: "Artillerymen, from 3rd Battalion, 320th Field Artillery Regiment, 3rd Brigade Combat Team, 101st Airborne Division (Air Assault), conduct M777A2 Howitzer training with precision guided munitions on Operational Base Fenty in Afghanistan. ",
            alt: "Artillerymen conduct M777A2 Howitzer training with precision guided munitions on Operational Base Fenty in Afghanistan.",
            homepage: ""
        }, {
            file_name: "0320",
            date: "March 20, 2015",
            by_line: "Photo by U.S. Air Force Staff Sgt. Nathan Lipscomb",
            caption: "U.S. Army Maj. Stewart Brown, 55th Signal Company, jumps from a UH-60 Black Hawk from 1st Battalion, 171st Aviation Regiment, during airborne operations at Plantation Airpark, Sylvania, Ga. Operation Skyfall is a joint, multilateral combat camera subject matter expert exchange, hosted by the 982nd Combat Camera Company, which takes place at multiple locations in Georgia.",
            alt: "United States Army Officer major jumps from a UH-60 Black Hawk helicopter.",
            homepage: ""
        }, {
            file_name: "0322",
            date: "March 22, 2015",
            by_line: "Photo by U.S. Army Pfc. Samantha Van Winkle",
            caption: "U.S. Army Soldiers, assigned to 1st Battalion, 27th Infantry Regiment, 2nd Stryker Brigade Combat Team, 25th Infantry Division, fire M795 projectile 155 mm rounds on Rodriguez Live Fire Complex, South Korea. Soldiers run a live-fire joint training exercise during Foal Eagle 2015. ",
            alt: "A United States Army tank fires M975 projectile 155mm rounds at a live-fire complex in South Korea.",
            homepage: ""
        }, {
            file_name: "0330_2",
            date: "March 30, 2015",
            by_line: "Photo by U.S. Army Sgt. Christopher R. Baker",
            caption: "U.S. Army Soldiers, from the 25th Infantry Division and soldiers from the Republic of Korea army participate in demolitions tactics and procedures during the joint exercise Foal Eagle near the DMZ, South Korea. ",
            alt: "U.S. Soldiers join soldiers from the Republic of Korea to participate in demolitions tactics and procedures during an exercise near the Korean Demilitarized Zone.",
            homepage: ""
        }],
        april: [{
            file_name: "0401_2",
            date: "April 1, 2015",
            by_line: "Photo by Department of Defense photographer EJ Hersom",
            caption: "U.S. Army athletes compete in the 100-meter wheelchair race during Army Trials at Fort Bliss in El Paso, Texas. Winners earned a spot on the Army’s 2015 Department of Defense Warrior Games team. ",
            alt: "U.S. Army athletes compete in the 100 meter wheelchair race for a spot on the Army’s DOD Warrior Games team.",
            homepage: ""
        }, {
            file_name: "0402",
            date: "April 2, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Bryan Henson",
            caption: "A U.S. Army 7th Special Forces Group (Airborne) Green Beret emerges from the Gulf of Mexico during a training exercise held in Santa Rosa Island, Fla. Green Berets recently conducted a waterborne infiltration of a mock outpost on the Santa Rosa Island training area. The exercise required a team of Special Forces Soldiers to conduct a surface swim onto the island and assault of the objective.",
            alt: "A United States Army Green Beret Special Forces Soldier emerges from the Gulf of Mexico during a training exercise. ",
            homepage: ""
        }, {
            file_name: "0402_2",
            date: "April 2, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Nicolas Morales",
            caption: 'After 85 rounds were fired from the modified howitzer, a U.S. Army Soldier, from the Presidential Salute Battery, 3rd U.S. Infantry Regiment (The Old Guard), renders a hand salute. This demonstration served multiple purposes. While keeping the Soldiers of the Presidential Salute Battery trained and proficient, it also served as an opportunity to showcase their very specific skill set to all in attendance.',
            alt: "After 85 rounds were fired from the modified howitzer, a Soldier from the Old Guard renders a hand salute. ",
            homepage: ""
        }, {
            file_name: "0411",
            date: "April 11, 2015",
            by_line: "Photo by U.S. Army Spc. Cody Wes Torkelson",
            caption: "An Old Guard Soldier performs as part of the 2015 National Cherry Blossom Festival Parade in Washington, D.C. The National Cherry Blossom Festival celebrates the spring season, and is one of the most attended events of the year in D.C.",
            alt: "An Old Guard Soldiers performs during the 2015 National Cherry Blossom Festival Parade in Washington, D.C.",
            homepage: ""
        }, {
            file_name: "0411_2",
            date: "April 11, 2015",
            by_line: "Photo by U.S. Air Force Staff Sgt. Sean Martin",
            caption: "U.S. Army and British paratroopers perform a static-line jump at Holland Drop Zone in preparation for Combined Joint Operational Access Exercise 15-01 on Fort Bragg, N.C. Combined Joint Operational Access Exercise 15-01 is an 82nd Airborne Division-led bilateral training event on Fort Bragg, N.C. It is the largest exercise of its kind held on Fort Bragg in nearly 20 years, and demonstrates interoperability between U.S. Army and British Army soldiers, U.S. Air Force, Air National Guard and Royal Air Force airmen and U.S. Marines.",
            alt: "U.S. Army and British paratroopers perform a static-line jump at Holland Drop Zone in preparation for Combined Joint Operational Access Exercise 15-01.",
            homepage: ""
        }, {
            file_name: "0412_2",
            date: "April 12, 2015",
            by_line: "Photo by U.S. Army photographer Patrick A. Albright",
            caption: "A U.S. Army Soldier competes during the 2015 Best Ranger Competition on Fort Benning, Ga. The competition started in 1982 as a way to honor Lt. Gen. David E. Grange Jr. and seeks to determine the best two-man team from the entire U.S. Armed Forces.",
            alt: "2015 Year in Pictures : A United States Army Soldier participates in the 2015 Best Ranger Competition.",
            homepage: ""
        }, {
            file_name: "0416",
            date: "April 16, 2015",
            by_line: "Photo by U.S. Air Force Senior Airman Christopher Reel",
            caption: "U.S. Army Soldiers, assigned to 3rd U.S. Infantry Regiment (The Old Guard), act as opposition forces for 2nd Brigade Combat Team, 82nd Airborne Division Soldiers, during Combined Joint Operational Access Exercise 15-01 on Fort Bragg, N.C. The exercise is a 82nd Airborne Division-led, bilateral training event at Fort Bragg and demonstrates interoperability between U.S. and U.K. army soldiers in addition to the United States Air Force, Air National Guard, U.S. Marine Corps and the Royal Air Force.",
            alt: "Soldiers assigned to The Old Guard act as opposition forces, firing rifles during a training exercise on Fort Bragg, North Carolina.",
            homepage: ""
        }, {
            file_name: "0420",
            date: "April 20, 2015",
            by_line: "Photo by U.S. Army Spc. Nikayla Shodeen",
            caption: "U.S. Army Capt. Kristen Griest, middle, carries a fellow Soldier as part of combative training during the Ranger Course on Fort Benning, Ga. Nineteen female Soldiers made history, April 20, when they began the first gender-integrated Ranger Course assessment.",
            alt: " United States Army Captain Kristen Griest, one of the first female Soldiers to complete the Ranger Course on Fort Benning, Ga., carries a male classmate.",
            homepage: ""
        }, {
            file_name: "0429_3",
            date: "April 27, 2015",
            by_line: "Photo by U.S. Army photographer John Pennell",
            caption: "A U.S. Army Soldier from D Company, 1st Battalion, 52nd Aviation Regiment stands outside a CH-47F Chinook helicopter at the Kahiltna Glacier base camp on Mount McKinley, Alaska.",
            alt: "An aviator stands outside a CH-47F Chinook helicopter at Kahiltna Glacier base camp on Mount McKinley, Alaska.",
            homepage: ""
        }, {
            file_name: "0429",
            date: "April 29, 2015",
            by_line: "Photo by U.S. Army Pfc. Eric Overfelt",
            caption: "Vice Chief of Staff of the Army Gen. Daniel B. Allyn presents Staff Sgt. Travis Dunn, Ranger squad leader, with the Bronze Star Medal with Valor and the Purple Heart. Dunn was wounded while conducting combat operations in Nangahar province, Afghanistan, Dec. 2, 2014",
            alt: "Vice Chief of Staff of the Army General Daniel B. Allyn presents an Army Ranger with the Bronze Star Medal for Valor and the Purple Heart.",
            homepage: ""
        }],
        may: [{
            file_name: "0506",
            date: "May 6, 2015",
            by_line: "Photo by Timothy L. Hale",
            caption: 'U.S. Army Staff Sgt. Dee McMurdo, representing the 84th Training Command, crosses the finish at "Little Nasty Nick" obstacle course during the 2015 U.S. Army Reserve Best Warrior Competition on Fort Bragg, N.C. The Army Reserve Best Warrior Competition determines the top noncommissioned officer and junior enlisted Soldier to represent the Army Reserve in the Department of the Army Best Warrior Competition on Fort Lee, Va.',
            alt: "Soldier competes in the U.S. Army Reserve Best Warrior Competition",
            homepage: ""
        }, {
            file_name: "0508",
            date: "May 8, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Bernardo Fuller",
            caption: "As the world marks the 70th anniversary of the end of World War II in Europe, known as Victory in Europe Day, dozens of World War II aircraft took to the skies over the District of Columbia, honoring those who fought in the war and those on the homefront who produced the tanks, ships, and aircraft that enabled the U.S. and its Allies to achieve victory.",
            alt: "A Veteran salutes in Washington, D.C. during the 70th anniversary to the end of WWII.",
            homepage: ""
        }, {
            file_name: "0519",
            date: "May 19, 2015",
            by_line: "Photo by U.S. Army Sgt. Jennifer Brady",
            caption: "For the first time since losing his legs in Afghanistan, U.S. Army Master Sgt. Cedric King joined his fellow 82nd Airborne Division paratroopers on a division run to kick off All American Week on Fort Bragg, N.C.",
            alt: "A Wounded Veteran participates in a division run during All American Week.",
            homepage: ""
        }, {
            file_name: "0520",
            date: "May 19, 2015",
            by_line: "Photo by Martin Greeson",
            caption: "A U.S. Army Soldier, assigned to Special Operations Command Europe, prepares to land after a qualification jump at Malmsheim airfield near Malmsheim, Germany. ",
            alt: "An Army Soldier from Special Operations Command Europe parachutes to the ground.",
            homepage: ""
        }, {
            file_name: "0521_2",
            date: "May 21, 2015",
            by_line: "Photo by Rachel Larue",
            caption: "U.S. Army Pfc. Johnny Allen, 3rd U.S. Infantry Regiment (The Old Guard), Charlie Company, places American flags at the headstones in Section 12 of Arlington National Cemetery during “Flags in,” in Arlington, Va. The Old Guard has conducted “Flags in,” when an American flag is placed at every headstone, since 1948. ",
            alt: "A private first class Soldier from The Old Guard places an American Flag at a grave.",
            homepage: ""
        }, {
            file_name: "0522",
            date: "May 22, 2015",
            by_line: "Photo by U.S. Army Sgt. James Avery",
            caption: 'Lithuanian Land Forces Soldiers from 2nd Coy, Iron Wolf Brigade, fire a smoke screen from an M113A1 Armored Personnel Carrier during a joint live-fire exercise with their American partners, U.S. Army unit Team Eagle, Task Force 2-7 Infantry, held at the Great Lithuanian Hetman Jonusas Radvila Training Regiment, in Rukla, Lithuania. As part of Atlantic Resolve, an ongoing multi-national partnership focused on joint training and security cooperation between the U.S. and other NATO allies, the Soldiers of Team Eagle work with, and around, their Lithuanian, Polish, Portuguese, French and German allies on a daily basis. ',
            alt: "A tank fires a smoke screen during a live-fire exercise between NATO allies.",
            homepage: ""
        }, {
            file_name: "0523",
            date: "May 23, 2015",
            by_line: "Photo by John Martinez",
            caption: "Nearly 1,000 cadets from the Class of 2015 graduated and commissioned during their graduation ceremony at Michie Stadium at West Point, N.Y. Retired Army Gen. Martin E. Dempsey, 18th chairman of the Joint Chiefs of Staff, was the commencement speaker for West Point’s 217th graduating class.",
            alt: "U.S. Army West Point cadets celebrate their graduation",
            homepage: ""
        }, {
            file_name: "0525",
            date: "May 25, 2015",
            by_line: "Photo by Rachel Larue",
            caption: "Genevieve Kynaston, age 3, carries roses through Section 60 of Arlington National Cemetery on Memorial Day in Arlington, Va. Arlington National Cemetery receives around 150,000 visitors over Memorial Day weekend.",
            alt: "A child carries roses in Arlington National Cemetery.",
            homepage: ""
        }, {
            file_name: "0526",
            date: "May 26, 2015",
            by_line: "Photo by U.S. Army Sgt. Edward French IV",
            caption: "U.S. Army Green Berets from 3rd Special Forces Group (Airborne), and Marines from Marine Special Operations Command, crawl across the Red Sea floor on a closed-circuit dive during Eager Lion 2015 in Jordan.",
            alt: "Green Berets crawl across the Red Sea floor.",
            homepage: ""
        }, {
            file_name: "0529",
            date: "May 29, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "U.S. Army chaplain candidates in the Basic Officer Leaders Course rappel down Victory Tower under the watchful eye of Soldiers of the 104th Training Division.",
            alt: "Chaplain Corps candidates rappel during a leaders course training.",
            homepage: ""
        }],
        june: [{
            file_name: "0601",
            date: "June 1, 2015",
            by_line: "Photo by U.S. Army Spc. Michelle U. Blesam",
            caption: "A U.S. Army Soldier, assigned to 1st Brigade, 1st Armored Division, fires a M136 AT4 during Decisive Action Rotation 15-08 at the National Training Center, Fort Irwin, Calif.",
            alt: "A U.S. Army Soldier fires a M136 AT4.",
            homepage: ""
        }, {
            file_name: "0602",
            date: "June 2, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Bernardo Fuller",
            caption: "President Barack Obama bestows the Medal of Honor to U.S. Army Sgt. William Shemin, accepting on his behalf are his daughters Elsie Shemin-Roth (middle) and Ina Bass (left), in the East Room of the White House. Shemin, a Jewish-American, distinguished himself as a member of 2nd Battalion, 47th Infantry Regiment, 4th Infantry Division, American Expeditionary Forces, during combat operations against the enemy on the Vesle River, near Bazoches, France, during World War I. ",
            alt: "President Barack Obama awards the Medal of Honor. ",
            homepage: ""
        }, {
            file_name: "0607",
            date: "June 7, 2015",
            by_line: "Photo by U.S. Air Force Master Sgt. Brian Bahret",
            caption: "George Shenkle, World War II veteran and former U.S. Army Soldier with Easy Company, 508th Parachute Infantry Regiment, 82nd Airborne Division, reacts as U.S. Army Soldiers parachute over the historic La Fiere drop zone near Sainte Mere Eglise, Normandy, France, to commemorate the 71st anniversary of D-Day. More than 380 U.S. service members from Europe and affiliated D-Day historical units participated in the 71st anniversary air drop as part of Joint Task Force D-Day 71. ",
            alt: "A WWII Veteran as Soldiers parachute over La Fiere drop zone to commemorate D-Day. ",
            homepage: ""
        }, {
            file_name: "0608",
            date: "June 8, 2015",
            by_line: "Photo by U.S. Marine Corps Sgt. Eric M. LaClair",
            caption: "U.S. service members, from the Defense POW/MIA Accounting Agency (DPAA), escort the recently disinterred remains of a service member, from the U.S.S. Oklahoma, previously buried as unknown.  The disinterment, taking place at the National Memorial Cemetery of the Pacific, Honolulu, Hawaii, was conducted in an effort to identify service members.  The mission of DPAA is to conduct global search, recovery and laboratory operations to provide the fullest possible accounting for our missing personnel to their families and the nation.",
            alt: "U.S. service members from the Defense Prisoner of War / Missing in Action Accounting Agency escort the recently disinterred remains of a service member.",
            homepage: ""
        }, {
            file_name: "0610",
            date: "June 10, 2015",
            by_line: "Photo by U.S. Army National Guard Staff Sgt. Roby Di Giovine",
            caption: "A UH-72A Lakota assigned to “A” Company, 2-151st Security and Support Aviation Battalion, South Carolina Army National Guard, flies over South Carolina’s major hurricane escape routes, Beaufort, S.C. The helicopter provided air support to local law enforcement, police agencies, and the South Carolina Emergency Management Division during a hurricane evacuation exercise. ",
            alt: "2015 Year in Pictures : A UH-72A Lakota helicopter flies over South Carolina’s hurricane escape routes.",
            homepage: ""
        }, {
            file_name: "0622",
            date: "June 16, 2015",
            by_line: "Photo by U.S. Army Spc. Tyler Kingsbury",
            caption: "U.S. Soldiers of 1st Battalion, 252nd Armored Regiment, move through fire while conducting fire phobia training during a Kosovo Force (KFOR) mission rehearsal exercise at the Joint Multinational Readiness Center in Hohenfels, Germany. The KFOR mission rehearsal exercise is based on the current operational environment, and is designed to prepare the unit for peace support, stability and contingency operations, in support of civil authorities, to maintain a safe and secure environment. ",
            alt: "U.S. Soldiers conduct fire phobia training during a Kosovo Force, or KFOR, mission rehearsal exercise",
            homepage: ""
        }, {
            file_name: "0617_5",
            date: "June 17, 2015",
            by_line: "Photo by U.S. Army Sgt. James Avery",
            caption: "U.S. Army Cpl. Zachary Lubbers, an infantryman assigned to Team Eagle, Task Force 2-7 Infantry, scans for targets from his foxhole during multinational training at the Great Lithuanian Hetman Jonusas Radvila Training Regiment. Saber Strike is a long-standing U.S. Army Europe-led cooperative training exercise. This exercise's objectives facilitate cooperation amongst the U.S., Estonia, Latvia, Lithuania and Poland to improve joint operational capability in a range of missions, as well as preparing the participating nations and units to support multinational contingency operations.",
            alt: "An Army Corporal infantryman scans for targets from his foxhole.",
            homepage: ""
        }, {
            file_name: "0621",
            date: "June 21, 2015",
            by_line: "Photo by U.S. Marine Corps Gunnery Sgt. Ezekiel R. Kitandwe",
            caption: "Members of the U.S. Army cycling team compete in the 2015 DOD Warrior Games aboard Marine Corps Base Quantico, Va. The Warrior Games, founded in 2010, is a Paralympic-style competition that features eight adaptive sports for wounded, ill, and injured service members and veterans from the U.S. Army, Marine Corps, Navy, Coast Guard, Air Force, Special Operations Command, and the British Armed Forces. ",
            alt: "U.S. Army bikers compete in the 2015 Warrior Games.",
            homepage: ""
        }, {
            file_name: "0624",
            date: "June 24, 2015",
            by_line: "Photo by U.S. Army Maj. Randy Stillinger",
            caption: "U.S. Army Sgt. Troy Lord, a CH-47 Flight Engineer with the Texas National Guard's 2-149th General Support Aviation Battalion, guides a CH-47 Chinook helicopter, as Soldiers from 1st Battalion, 133rd Field Artillery, hook up a 105mm Howitzer during an air assault exercise on Fort Hood.",
            alt: "Army National Guardsmen from Texas hook up a Howitzer from inside a Chinook helicopter.",
            homepage: ""
        }, {
            file_name: "0626",
            date: "June 26, 2015",
            by_line: "Photo by Sherman Hogue",
            caption: "A U.S. Army‬ UH-60 Blackhawk helicopter crew chief, assigned to the Alaska National Guard, conducts water bucket operations during a fire-fighting mission south of Tok, Alaska. Two UH-60 helicopters aided in fighting the wildfire in coordination with the Bureau of Land Management, Fire Services based out of Fort Wainwright, Alaska.",
            alt: "A Soldier from the Alaska National Guard fights a fire from the inside of a UH-60 Black Hawk helicopter.",
            homepage: ""
        }],
        july: [{
            file_name: "0703",
            date: "July 3, 2015",
            by_line: "Photo by U.S. Army Capt. Charlie Emmons",
            caption: 'A group of 1st Battalion, 187th Infantry Regiment, 3rd Brigade Combat Team, Soldiers sprint to their objective during platoon live-fire training on Tactical Base Gamberi in eastern Afghanistan.',
            alt: "United States Army Soldiers sprint with their weapons during live-fire training in Afghanistan.",
            homepage: ""
        }, {
            file_name: "0704",
            date: "July 4, 2015",
            by_line: "Photo by U.S. Army Chief Warrant Officer 2 Ryan Boas",
            caption: "A U.S. Army Soldier trains his working dog on Bagram Airfield, Afghanistan. The Soldier is assigned to the 709th Military Police Battalion, 18th Military Police Brigade.",
            alt: "A United States Army military working dog handler trains his working dog on Bagram Airfield, Afghanistan.",
            homepage: ""
        }, {
            file_name: "0704_2",
            date: "July 4, 2015",
            by_line: "Photo by U.S. Army Spc. Cody W. Torkelson",
            caption: "The Presidential Salute Battery, 3rd U.S. Infantry Regiment (The Old Guard), supports the Capitol Fourth Concert, honoring the nation as part of the Fourth of July celebration at Capitol Hill in Washington D.C. ",
            alt: "An Army Soldier from The Old Guard stands at attention in front of the U.S. Capitol building in Washington, D.C.",
            homepage: ""
        }, {
            file_name: "0716_2",
            date: "July 16, 2015",
            by_line: "Photo by U.S. Air National Guard Tech. Sgt. Amy M. Lovgren",
            caption: "U.S. Army firefighters, assigned to the Nebraska Army National Guard, conduct aircraft rescue firefighting training, at Volk Field Combat Readiness Training Center, Wis., in preparation for an upcoming exercise.",
            alt: "U.S. Army firefighters from the Nebraska Army National Guard conduct firefighting training.",
            homepage: ""
        }, {
            file_name: "0717",
            date: "July 17, 2015",
            by_line: "Photo by U.S. Army Master Sgt. Michel Sauret",
            caption: "A U.S. Army Reserve combat engineer Soldier, from the 374th Engineer Company (Sapper), tries to stand up with a loaded ruck sack full of water during Combat Water Survival Training at Fort Hunter Liggett, Calif.",
            alt: "2015 Year in Photos : An Army Reserve Soldier emerges from a pool during combat water survival training.",
            homepage: ""
        }, {
            file_name: "0717_2",
            date: "July 17, 2015",
            by_line: "Photo by U.S. Army Master Sgt. Michel Sauret",
            caption: "U.S. Army Reserve combat engineer Soldiers, from the 374th Engineer Company (Sapper), headquartered in Concord, Calif., performed a helicopter casting into Lopez Lake, Calif., during a two-week field exercise known as a Sapper Leader Course Prerequisite Training at Camp San Luis Obispo Military Installation, Calif. The unit is grading its Soldiers on various events to determine which ones will earn a spot on a \"merit list\" to attend the Sapper Leader Course at Fort Leonard Wood, Mo. ",
            alt: "Two Army Reserve combat engineer Soldiers jump from a helicopter into Lopez Lake in California.",
            homepage: ""
        }, {
            file_name: "0719_3",
            date: "July 19, 2015",
            by_line: "Photo by Tim Hipps",
            caption: "U.S. Army Spc. Nathan Schrimsher, of the U.S. Army World Class Athlete Program, defeats Emmanual Zapata of Argentina in a fencing bonus round bout en route to earning a berth in the 2016 Olympic Games. Schrimsher earned a third-place finish in men's modern pentathlon at the 2015 Pan American Games in Toronto.",
            alt: "A specialist in the U.S. Army defeats their opponent in a fencing match en route to earning a berth in the 2016 Olympic Games.",
            homepage: ""
        }, {
            file_name: "0722",
            date: "July 22, 2015",
            by_line: "Photo by U.S. Army Sgt. Ken Scar",
            caption: "A U.S. Army Reserve Soldier reads some of the 58,272 names etched into \"The Wall\" of the Vietnam Veterans Memorial in Washington, D.C. ",
            alt: "An Army Reserve Soldier stands in front of the Vietnam Veterans Memorial in Washington, D.C.",
            homepage: ""
        },{
            file_name: "0729_3",
            date: "July 29, 2015",
            by_line: "Photo by U.S. Marine Corps Sgt. James Gulliver",
            caption: "U.S. Army Sgt. Jonathon Bailey shouts instructions while performing push-ups aboard the Military Sealift Command joint high-speed vessel, USNS Millinocket (JHSV 3), during a physical training session designed to help with stress management.",
            alt: "A sergeant completes pushups during a physical training session.",
            homepage: ""
        }, {
            file_name: "0731",
            date: "July 31, 2015",
            by_line: "Photo by U.S. Army Master Sgt. Michel Sauret",
            caption: "A wave splashes onto an MK-2 Boat operated by U.S. Army Reserve Soldiers Staff Sgt. Chad Bentley, bridge crew member, and Spc. Ben Adams, medic, from 341st Engineer Company (Multi-Role Bridge), during a sling loading operation on the Arkansas River near Fort Chaffee. Soldiers from various Army Reserve and active duty units trained together at River Assault, a bridging training exercise that involves Army Engineers and other support elements. ",
            alt: "Two soldiers aboard an MK-2 Boat crash into waves.",
            homepage: ""
        }],
        august: [{
            file_name: "0801",
            date: "Aug. 1, 2015",
            by_line: "U.S. Army photo",
            caption: "A CH-47D Chinook helicopter prepares to release water from a large bucket onto the Rocky Fire near Clear Lake, Calif.",
            alt: "A CH-47D Chinook helicopter releases water from a large bucket onto the Rocky Fire in California.",
            homepage: ""
        }, {
            file_name: "0805",
            date: "Aug. 5, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Jason Hull",
            caption: "Snipers from the 1st Squadron, 11th Armored Cavalry Regiment, dash across the rocky desert terrain during a combined-arms live-fire exercise at Fort Irwin, Calif. The 11th Armored Cavalry Regiment was one of several organizations from across the U.S. participating in Operation Dragon Spear, a demonstration which included a joint forcible entry operation with XVIII Airborne Corps, 82nd Airborne Division, 75th Ranger Regiment, 10th Special Forces Group and the Air Force.",
            alt: "U.S. Army snipers wearing special camouflage train in a life-fire exercise.",
            homepage: ""
        }, {
            file_name: "0806",
            date: "Aug. 6, 2015",
            by_line: "Photo by U.S. Army Spc. Marcus Floyd",
            caption: "U.S. Army Sgt. David Pileggi, a Soldier with P Troop, 4th Squadron, 2nd Cavalry Regiment, paddles across a lake at the Nowa Deba Training Area in Poland. The Soldiers are completing a water obstacle course created by Polish soldiers from the 6th Airborne Brigade. The training is part of Operation Atlantic Resolve, an ongoing multinational partnership focused on combined training and security cooperation between NATO allies. ",
            alt: "Soldiers swimming across a lake in Poland during water obstacle training.",
            homepage: ""
        }, {
            file_name: "0809",
            date: "Aug. 9, 2015",
            by_line: "Photo by U.S Army National Guard Staff Sgt. Eddie Siguenza",
            caption: "U.S. Army Soldiers from Task Force Alpha, California Army National Guard, remove a burnt tree stump to extinguish a fire near the mountains of Wildcat Butte, Humboldt County, Calif., during the Humboldt Lightning Fire. In less than two weeks, the Humboldt Lightning Fire scorched 4,700 acres",
            alt: "Soldiers from the California Army National Guard fight the Humboldt Lightning Fire.",
            homepage: ""
        }, {
            file_name: "0816",
            date: "Aug. 16, 2015",
            by_line: "Photo by U.S. Army Spc. Christopher A. Hernandez",
            caption: "Firefighters from multiple engineer detachments utilize a high-pressure fire hose to extinguish a flame during a training event as part of the Combat Support Training Exercise on Fort McCoy, Wis. The training replicates real-world missions, which develops the units’ abilities to successfully plan, prepare, and provide combat service support.",
            alt: "Firefighters using a high-pressure firehose to put out a large fire during training.",
            homepage: ""
        }, {
            file_name: "0816_2",
            date: "Aug. 16, 2015",
            by_line: "Photo by U.S. Army Spc. Christopher M. Blanton",
            caption: "U.S. Army Soldiers, assigned to the Idaho National Guard's 116th Cavalry Brigade Combat Team, calibrate a Paladin M109A6 155mm Artillery System during Decisive Action Rotation 15-09 at the National Training Center, Fort Irwin, Calif. The NTC provides Soldiers an opportunity to hone their skills in an immersive, realistic environment.",
            alt: "Soldiers from the Idaho National Guard calibrate a Paladin M109A6 155mm Artillery System.",
            homepage: ""
        }, {
            file_name: "0821",
            date: "August 21, 2015",
            by_line: "Photo by Patrick A. Albright",
            caption: "Graduates of U.S. Army Ranger Class 08-15, render a salute during their graduation on Fort Benning, Ga. U.S. Army Capt. Kristen Griest and 1st Lt. Shaye Haver became the first female Soldiers to earn the Ranger tab.",
            alt: "Two female Ranger School graduates, Capt. Kristen Griest and First Lt. Shaye Haver salute during their graduation ceremony.",
            homepage: ""
        }, {
            file_name: "0821_2",
            date: "Aug. 21, 2015",
            by_line: "Photo by U.S. Army National Guard Sgt. Matthew Sissel",
            caption: "A U.S. Army Soldier, assigned to the Washington National Guard, uses his body to create an opening in a wire obstacle so his team can assault a position during Exercise Grizzly Defender, Alberta, Canada. Grizzly Defender is a joint training exercise with the Canadian Army Reserve with a focus on offensive tasks including patrols, convoys, raids, information operations, traffic control points, and company-level group attacks.",
            alt: "Soldiers from the Washington National guard crawl under a barbed wire obstacle while holding their rifles.",
            homepage: ""
        }, {
            file_name: "0823_2",
            date: "Aug. 23, 2015",
            by_line: "Photo by U.S. Army 1st Lt. Krista Yaglowski",
            caption: "Soldiers, assigned to Kosovo Force Multinational Battle Group-East's Task Force Hurricane, made up of Soldiers from the National Guard and U.S. Army Reserve, hike to the peak of Mount Ljuboten in southern Kosovo. Led by German members of MNBG-E's headquarters, these Soldiers earned Germany's Edelweiss Badge by successfully completing the 8,000 foot trek.",
            alt: "Soldiers from the National Guard and Army Reserve hike the peak of Mount Ljuboten in southern Kosovo.",
            homepage: ""
        }, {
            file_name: "0826",
            date: "Aug. 26. 2015",
            by_line: "Photo by U.S. Army Capt. Lisa Beum",
            caption: "More than 1,000 paratroopers from Germany, Italy, the Netherlands, Poland, the U.K and the U.S. with Task Force Devil, 82nd Airborne Division, land on Hohenburg drop zone executing a Joint Forcible Entry during exercise Swift Response 15 - the U.S. Army’s largest combined airborne training event in Europe since the end of the Cold War - in Hohenfels, Germany.",
            alt: "Paratroopers descend during the U.S. Army’s largest combined airborne training event in Europe since the end of the Cold War.",
            homepage: ""
        }],
        september: [{
            file_name: "0903",
            date: "Sept. 3, 2015",
            by_line: "Photo by Elena Baladelli",
            caption: "U.S. Army chemical, biological, radiological and nuclear defense from Headquarters Company, 54th Brigade Engineer Battalion, 173rd Airborne Brigade, inspect a mock contaminated subway station in Rieti, Italy, during bilateral exercise Toxic Dragon 15. Toxic Dragon brought together chemical, biological, radiological and nuclear troops from both the U.S. and Italy to share techniques and improve interoperability.",
            alt: "Soldiers conduct CBRNE training on a mock contaminated subway station.",
            homepage: ""
        }, {
            file_name: "0903_2",
            date: "Sept. 3, 2015",
            by_line: "Photo by Richard Rzepka",
            caption: "The Soldiers of 1st Battalion, 1st Special Forces Group, are well versed in adapting to the art of war and honed their hand-to-hand combat skills at Torii Station, Okinawa, during Fight Night VI – a battalion-level Modern Army Combative competition.",
            alt: "U.S. Army Soldiers fighting, practicing hand-to-hand combat skills.",
            homepage: ""
        }, {
            file_name: "0908",
            date: "Sept. 8, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Christopher Freeman",
            caption: "Soldiers from 3rd Special Forces Group move a simulated casualty to a UH-60 Black Hawk assigned to Charlie Company, 3rd General Support Aviation Battalion, 82nd Combat Aviation Brigade, during partnered training on Fort Bragg, N.C. Charlie Company, known as \"All-American DUSTOFF\" serves as the only aerial medical evacuation team in the 82nd Airborne Division.",
            alt: "Special Forces Soldiers medevac a simulated casualty to a UH-60 Black Hawk helicopter.",
            homepage: ""
        }, {
            file_name: "0909",
            date: "Sept. 9, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "U.S. Army Staff Sgt. Russell Vidler leaps over the wall at the Fit to Win obstacle course on Fort Jackson, S.C. Vidler, a Reserve drill sergeant assigned to the 98th Training Division, was in a head-to-head competition for the title of Army Reserve's top drill sergeant.",
            alt: "A U.S. Army staff sergeant from the Army Reserves jumps over an obstacle while holding his rifle.",
            homepage: ""
        }, {
            file_name: "0913",
            date: "Sept. 13, 2015",
            by_line: "Photo by U.S. Army Spc. Alexander Rector",
            caption: 'New York Army National Guard Sgt. Aaron Lawrence and Spc. Zach Bouley, two infantrymen assigned to Troop C, 2nd Squadron, 101st Cavalry Regiment, recon a landing site in advance of an inbound F470 Combat Rubber Raiding Craft during a training exercise in Buffalo, N.Y. During the exercise, Soldiers from the troop conducted waterborne operations, and practiced covertly deploying to a simulated enemy beachhead.',
            alt: "Two Soldiers from the New York Army National Guard conduct waterborne operations while holding M16 model A2 rifles.",
            homepage: ""
        }, {
            file_name: "0914",
            date: "Sept. 14, 2015",
            by_line: "Photo by Gertrud Zach",
            caption: "Officers and noncommissioned officers from units throughout U.S. Army Europe conduct the Team Reaction Lane during the 2015 European Best Warrior Competition, at 7th Army Joint Multinational Training Command's Grafenwoehr Training Area, Germany. Some 22 candidates participated in the annual weeklong competition, the most prestigious competitive event of the region. ",
            alt: "United States Army Soldiers compete in the 2015 European Best Warrior Competition.",
            homepage: ""
        }, {
            file_name: "0917",
            date: "Sept. 17, 2015",
            by_line: "Photo by Department of Defense photographer Glenn Fawcett",
            caption: "Secretary of Defense Ash Carter pins the Soldier's medal on U.S. Army Spc. Alek Skarlatos, during a ceremony at the Pentagon, for his role in stopping a gunman on a Paris-bound train outside of Brussels. ",
            alt: " SECDEF Ashton Carter pins the Soldier’s medal on Specialist Alek Skarlatos.",
            homepage: ""
        }, {
            file_name: "0920",
            date: "Sept. 20, 2015",
            by_line: "Photo by U.S. Air Force Staff Sgt. Cody H. Ramirez",
            caption: "U.S. Army Staff Sgt. Brian Wright, 1st Battalion, 1st Special Operations Group (Airborne), holds a U.S. flag outside a C-130 Hercules during the 2015 Japanese-American Friendship Festival at Yokota Air Base, Japan. The U.S. flag, waved by Wright, was held out on one C-130 and a Japanese flag was simultaneously held outside another taxiing aircraft, symbolizing the U.S. and Japan partnership and reinforcing the general idea of the festival – to increase bilateral relationships between the two countries. ",
            alt: "A Staff Sergeant holds an American Flag out of the top of a C-130 Hercules while taxiing.",
            homepage: ""
        }, {
            file_name: "0929",
            date: "Sept. 29, 2015",
            by_line: "U.S. Army photo",
            caption: "Soldiers take to the water by jumping out of Black Hawk helicopters in an airborne operation, helicopter cast and recovery, known as helocast, off the shores of Okinawa‬.‬ The operation involves inserting and extracting personnel or equipment from a helicopter over water. Helocasting is an effective means of inserting or extracting reconnaissance elements. Units plan and conduct a helocast operation similar to an air movement operation, except the landing zone is in the water. ",
            alt: " Soldiers jump out of a UH-60 Black Hawk helicopter off the shores of Okinawa.",
            homepage: "2015 Year in Pictures : Soldiers jump out of a UH-60 Black Hawk helicopter off the shores of Okinawa."
        }, {
            file_name: "0930",
            date: "Sept. 30, 2015",
            by_line: "Photo by Paolo Bovo",
            caption: "Paratroopers from 173rd Brigade Support Battalion, 173rd Airborne Brigade, prepare to board a 12th Combat Aviation Brigade CH-47 Chinook helicopter for an airborne operation, at Juliet Drop Zone, in Pordenone, Italy. The 173rd Airborne Brigade is the U.S. Army Contingency Response Force in Europe, capable of projective forces anywhere in the U.S. European, Africa or Central Command areas of responsibility within 18 hours. ",
            alt: "Paratroopers prepare to board a CH-47 Chinook helicopter.",
            homepage: ""
        }],
        october: [{
            file_name: "1001_3",
            date: "Oct. 1, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "U.S. Army Soldiers in Basic Combat Training low crawl through the final obstacle during the Fit to Win endurance course at Fort Jackson, S.C.",
            alt: "2015 Year in Pictures : Soldiers in Basic Combat Training low crawl through an obstacle during the Fit to Win endurance course.",
            homepage: ""
        }, {
            file_name: "1014",
            date: "Oct. 14, 2015",
            by_line: "Photo by U.S. Air Force Staff Sgt. Gregory Brook",
            caption: "A Scout Sniper Team Marksman, part of the Recon Platoon from Bravo Company, 2nd Battalion, 124th Infantry Regiment, 53rd Brigade Combat Team, Florida National Guard, fires a Barrett M107 .50 caliber semi-automatic anti-materiel rifle at a 1200-meter target during a live-fire long-range marksmanship training and qualification course at the Arta training range in Djibouti.",
            alt: "A Scout Sniper Team Marksman, fires a Barrett M107 .50 caliber semi-automatic anti-materiel rifle.",
            homepage: ""
        }, {
            file_name: "1017",
            date: "Oct. 17, 2015",
            by_line: "Photo by U.S. Army Sgt. Brandon Banzhaf",
            caption: "U.S. Army Soldiers conduct a live-fire demonstration with M1A2 Abrams tanks and an M2A3 Bradley Fighting Vehicle on Fort Hood, Texas. The Soldiers are assigned to 3rd Brigade Combat Team, 1st Cavalry Division.",
            alt: "Soldiers conduct a live-fire demonstration with M1A2 Abrams tanks and an M2A3 Bradley Fighting Vehicle.",
            homepage: ""
        }, {
            file_name: "1019",
            date: "Oct. 19, 2015",
            by_line: "Photo by U.S. Army Sgt. Terry Rajsombath",
            caption: "A U.S. Army Soldier holds up concertina wire for his fellow teammates to low crawl under during the European Best Squad Competition held at the 7th Army’s Joint Multinational Training Command’s, Grafenwoehr training area, Bavaria, Germany. The competition is multinational by design, and involves units from 14 nations with 18 squads competing for the right to be Europe’s best squad.",
            alt: "A Soldier holds up concertina wire for his fellow teammates to low crawl under during the European Best Squad Competition.",
            homepage: ""
        }, {
            file_name: "1019_2",
            date: "Oct. 19, 2015",
            by_line: "Photo courtesy of 725th Brigade Support Battalion",
            caption: "A U.S. Army Soldier with the 725th Brigade Support Battalion re-enlists atop a small unit support vehicle.",
            alt: "A Soldier with the 725th Brigade Support Battalion re-enlists atop a small unit support vehicle.",
            homepage: ""
        }, {
            file_name: "1021",
            date: "Oct. 21, 2015",
            by_line: "Photo by U.S. Army Sgt. Anthony Hewitt",
            caption: "Paratroopers, assigned to 307th Engineer Battalion, 3rd Brigade Combat Team, 82nd Airborne Division, begin paddling for a boat competition during the unit's annual \"Crossing of the Wall River\" event at Fort Bragg, N.C. Seven teams from the battalion crossed Kiest Lake to replicate the five trips across the Waal River. ",
            alt: "Paratroopers paddling during a boat competition during the unit's annual \"Crossing of the Wall River\" event.",
            homepage: ""
        }, {
            file_name: "1023",
            date: "Oct. 23, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Joe Abeln",
            caption: "Members of the U.S. Army Golden Knights Vertical Formation Skydiving Team complete a practice jump during day one of the U.S. Nationals competition.",
            alt: "The U.S. Army Golden Knights Vertical Formation Skydiving Team complete a practice jump during day one of the U.S. Nationals competition",
            homepage: ""
        }, {
            file_name: "1023_2",
            date: "Oct. 23, 2015",
            by_line: "Photo by U.S. Air Force Senior Master Sgt. Adrian Cadiz",
            caption: "Secretary of Defense Ash Carter applauds Army Secretary John M. McHugh after McHugh delivered farewell remarks during a farewell ceremony honoring him on Joint Base Myer-Henderson Hall, Va.",
            alt: "SecDef Ash Carter applauds Army Secretary John M. McHugh after McHugh delivered farewell remarks during a farewell ceremony.",
            homepage: ""
        }, {
            file_name: "1025",
            date: "Oct. 25, 2015",
            by_line: "Photo by U.S. Marine Corps Sgt. Justin Boling",
            caption: "Trevor LaFontaine, completes the 40th Marine Corps Marathon at Arlington, Va. LaFontaine, the first male finisher, finished the 26.2-mile race in 2:24. This year’s participants joined the 512,370 runners, who have completed the “Marathon of the Monument” since its inaugural event in 1976.",
            alt: "Trevor LaFontaine completes 40th Marine Corps Marathon.",
            homepage: ""
        }, {
            file_name: "1028",
            date: "Oct. 28, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "After losing his balance on a rope bridge obstacle, a Soldier in Basic Combat Training with E Company, 2nd Battalion, 39th Infantry Regiment, attempts to complete the task, using pure upper-body strength to get across, with his drill sergeant looking on. Soldiers in basic combat training must complete all tasks at Victory Tower on Fort Jackson, S.C., which also boasts a 40-foot rappelling wall, in order to graduate.",
            alt: "A Soldier in Basic Combat Training attempts an obstacle as an Army drill sergeant watches.",
            homepage: ""
        }],
        november: [{
            file_name: "1102",
            date: "Nov. 2, 2015",
            by_line: "Photo by NATO photographer Davide Passone",
            caption: "U.S. artillery elements take part in a live-fire in Capo Teulada, Sardinia, Italy, during NATO exercise Trident Juncture 15.",
            alt: "Year in Photos : U.S. artillery elements take part in a live-fire in Capo Teulada, Sardinia, Italy, during NATO exercise Trident Juncture 15.",
            homepage: ""
        }, {
            file_name: "1103",
            date: "Nov. 3, 2015",
            by_line: "Photo by U.S. Air National Guard Staff Sgt. Christopher S. Muncy",
            caption: "A U.S. Army Green Beret takes one knee during a noncombatant evacuation exercise, as part of Southern Strike 16, on Meridian Naval Air Station, Miss.",
            alt: "An Army Green Beret takes one knee during a noncombatant evacuation exercise as part of Southern Strike 16.",
            homepage: ""
        }, {
            file_name: "1104",
            date: "Nov. 4, 2015",
            by_line: "Photo by U.S. Air National Guard Staff Sgt. Christopher S. Muncy",
            caption: "U.S. Army Soldiers from Charlie Company, 2/20 Special Forces Group, fire various types of mortars at training targets during Exercise Southern Strike 16, at Camp Shelby Joint Forces Training Center, Miss. Exercise Southern Strike 16 emphasizes air-to-air, air-to-ground and special operations forces training opportunities.",
            alt: "Soldiers from Charlie Company, fire mortars at training targets during Exercise Southern Strike 16, at Camp Shelby Joint Forces Training Center.",
            homepage: ""
        }, {
            file_name: "1105",
            date: "Nov. 5, 2015",
            by_line: "Photo by U.S. Army Master Sgt. Michel Sauret",
            caption: "U.S. Army Cpl. Brittany Montana, a U.S. Army Reserve Soldier with the 354th Military Police Company, pulls the charging handle of an M2 Browning .50-caliber machine gun during weapons training on Camp Atterbury, Ind.",
            alt: "A U.S. Army Reserve Soldier pulls the charging handle of an M2 Browning .50-caliber machine gun.",
            homepage: ""
        }, {
            file_name: "1111_2",
            date: "Nov. 11, 2015",
            by_line: "Photo by Rachel Larue",
            caption: 'A Tomb Sentinel, 3rd U.S. Infantry Regiment (The Old Guard), guards the Tomb of the Unknown Soldier in Arlington National Cemetery in Arlington, Va. Wreaths laid for Veterans Day rest near the tomb.',
            alt: "A Tomb Sentinel, from The Old Guard, guards the Tomb of the Unknown Soldier in Arlington National Cemetery.",
            homepage: ""
        }, {
            file_name: "1112",
            date: "Nov. 12, 2015",
            by_line: "Photo by U.S. Air Force Senior Airman Westin Warburton",
            caption: "U.S. Army Capt. John Dills, Joint Task Force-Bravo tactical officer in charge, shakes hands with a boy during a medical readiness training exercise in San Jose De Rio Pinto, Honduras. The MEDRETEs JTF-B support provide military members with essential training in austere locations and helps build local community relations in the host country.",
            alt: "A U.S. Army captain shakes the hand of a little boy.",
            homepage: ""
        }, {
            file_name: "1113",
            date: "Nov. 12, 2015",
            by_line: "Photo by John Martinez",
            caption: "Retired U.S. Army Capt. Florent Groberg was inducted into the Pentagon's Hall of Heroes, Nov. 12, 2015, for conspicuous gallantry in Asadabad, Kunar Province, Afghanistan, Aug. 8, 2012. An attendee reads the details of the actions that took place on that day.",
            alt: "Retired Capt. Florent Groberg, medal of honor recipient, was inducted into the Pentagon's Hall of Heroes.",
            homepage: ""
        }, {
            file_name: "1124",
            date: "Nov. 24, 2015",
            by_line: "Photo by U.S. Army Sgt. 1st Class Brian Hamilton",
            caption: "U.S. Army 1st Sgt. Ricardo Gutierrez, MEDDAC, low crawls under the wire obstacle on the confidence course during the assessment phase of the Best Ranger competition held on Fort Jackson, S.C. Gutierrez is one of three Soldiers from Fort Jackson fighting for a position on a two-Soldier team that will represent the post during the 33rd annual competition taking place at Fort Benning, Ga., in April of next year.",
            alt: "A U.S. Army Soldier low crawls under a barbed-wire obstacle on the confidence course during the assessment phase of the Best Ranger competition.",
            homepage: ""
        }, {
            file_name: "1126",
            date: "Nov. 26, 2015",
            by_line: "Photo by U.S. Air Force Tech. Sgt. Robert Cloys",
            caption: "Florida Army National Guard members deployed from the 1st Battalion, 265th Air Defense Artillery Regiment in Palm Coast, Fla., play Florida Air National Guard members deployed from the 290th Joint Communications Support Squadron at MacDill Air Force Base, Fla., in a friendly \"Turkey Bowl\" football game at Bagram Air Field, Afghanistan. The Army National Guard team won the game 42-35",
            alt: "Florida Army National Guard Soldiers play football while deployed.",
            homepage: ""
        }, {
            file_name: "1128",
            date: "Nov. 28, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Brian Ragin",
            caption: "A paratrooper, assigned to the 716th Explosive Ordnance Disposal Detachment, 725th Brigade Support Battalion (Airborne), 4th Infantry Brigade Combat Team (Airborne), 25th Infantry Division, hugs a family member, after returning from a nine month deployment to Kuwait, at Joint Base Elmendorf-Richardson, Alaska.",
            alt: "A paratrooper assigned wearing an EOD patch, hugs a family member after returning from deployment.",
            homepage: ""
        }],
        december: [{
            file_name: "1202",
            date: "Dec. 2, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Michael Behlin",
            caption: "U.S. Army Spc. Timmy Racke, an infantryman with the 3rd Battalion, 69th Armor Regiment, 1st Armor Brigade Combat Team, 3rd Infantry Division, engages targets with his M240B Machine Gun while conducting battle drills at Pabrade Training Area, Lithuania.",
            alt: "A U.S. Army specialist Soldier infantryman fires an M240B Machine Gun.",
            homepage: ""
        },{
            file_name: "1202_2",
            date: "Dec. 2, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Armando R. Limon",
            caption: "U.S. Army Sgt. Eric Moreno, squad leader, Company B, 2nd Battalion, 35th Infantry Regiment, 3rd Brigade Combat Team, 25th Infantry Division, goes over a map of a landing site at Wheeler Army Airfield, Hawaii. Moreno’s company flew aboard UH-60 Black Hawk helicopters to conduct air assault operations at Marine Corps Training Area Bellows.",
            alt: " A Soldier goes over a map of a landing site at Wheeler Army Airfield, Hawaii.",
            homepage: ""
        }, {
            file_name: "1207",
            date: "Dec. 7, 2015",
            by_line: "Photo by U.S. Air Force Staff Sgt. Douglas Ellis",
            caption: "A U.S. Army Special Operations Forces parachute team member descends toward Luzon Drop Zone, during the 18th Annual Randy Oler Memorial Operation Toy Drop, hosted by U.S. Army Civil Affairs and Psychological Operations Command (Airborne) at Camp Mackall, N.C. Operation Toy Drop is the world's largest combined airborne operation and provides Soldiers the opportunity to help children in need receive toys for the holidays.",
            alt: "U.S. Army special operations forces parachute toward Luzon Drop Zone during 18th Annual Randy Oler Memorial Operation Toy Drop.",
            homepage: ""
        }, {
            file_name: "1208_2",
            date: "Dec. 8, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Daniel Love",
            caption: "U.S. Army Staff Sgt. Timothy Bennett, a cavalry scout with Blackjack Troop, 1st Squadron, 40th Cavalry Regiment, 4th Infantry Brigade Combat Team (Airborne), 25th Infantry Division, packs his parachute up after a night airborne training jump at Joint Base Elmendorf-Richardson, Alaska. ",
            alt: "A U.S. Army Soldier packs his parachute up after a night airborne training jump.",
            homepage: ""
        }, {
            file_name: "1210",
            date: "Dec. 10, 2015",
            by_line: "Photo by U.S. Army Staff Sgt. Alex Manne",
            caption: "Members of the U.S. Army Parachute Team, the Black Knights, the U.S. Army Special Operations Command Parachute Team, the Black Daggers and Dutch Jumpmasters conduct a military free fall airborne operation from a C-27 during Operation Toy Drop at Camp Mackall, N.C. Hosted by U.S. Army Civil Affairs and Psychological Operations Command (Airborne), Operation Toy Drop is the world’s largest combined airborne operation with seven partner-nation paratroopers participating that allows Soldiers the opportunity to help children in need everywhere receive toys for the holidays.",
            alt: " U.S. Army Parachute Team, the Black Knights, the U.S. Army Special Operations Command Parachute Team, the Black Daggers and Dutch Jumpmasters conduct a military free fall airborne operation from a C-27 during Operation Toy Drop.",
            homepage: ""
        }, {
            file_name: "1210_3",
            date: "Dec. 10, 2015",
            by_line: "Photo by U.S. Army 1st Lt. James Gallagher",
            caption: "U.S. Army Soldiers, assigned to 1/25 SBCT \"Arctic Wolves\", U.S. Army Alaska, transport equipment using snowshoes and ahkio sleds during an arctic mobility squad competition in the Yukon Training Area, Fort Wainwright, Alaska. ",
            alt: "U.S. Army Soldiers, transport equipment using snowshoes and ahkio sleds during an arctic mobility squad competition in the Yukon Training Area.",
            homepage: ""
        }, {
            file_name: "1212_2",
            date: "Dec. 12, 2015",
            by_line: "Photo courtesy of the U.S. Military Academy",
            caption: "Cadets cheering at the Army-Navy Game at Lincoln Financial Field, Philadelpha, Pa., Dec. 12, 2015.",
            alt: "Army-Navy game, West Point Cadets cheering at the Army/Navy Game at Lincoln Financial Field, Philadelpha, Pa.",
            homepage: ""
        }, {
            file_name: "1215",
            date: "Dec. 15, 2015",
            by_line: "Photo by U.S. Army Pfc. Emily Houdershieldt",
            caption: "A U.S. Soldier, assigned to the 18th Military Police Brigade, 21st Theater Sustainment Command, provides security during a convoy exercise on Grafenwoehr Training Area, Germany.",
            alt: " A U.S. Soldier provides security during a convoy exercise on Grafenwoehr Training Area, Germany.",
            homepage: ""
        }, {
            file_name: "1216",
            date: "Dec. 16, 2015",
            by_line: "Photo by U.S. Air Force Senior Airman Cory D. Payne",
            caption: "U.S. Army Soldiers assigned to Bravo Company, 2-124th Infantry Battalion, Combined Joint Task Force-Horn of Africa (CJTF-HOA), fire a M224 mortar during a live-fire exercise in Djibouti. Through unified action with U.S. and international partners in East Africa, CJTF-HOA conducts security force assistance, executes military engagement, provides force protection and provides military support to regional counter-violent extremist organization operations in order to support aligned regional efforts, ensure regional access and freedom of movement and protect U.S. interests.",
            alt: "Soldiers fire a M224 mortar during a live-fire exercise in Djibouti.",
            homepage: ""
        }, {
            file_name: "1229",
            date: "Dec. 29, 2015",
            by_line: "Photo by U.S. Army Cpl. Cody W. Torkelson",
            caption: "U.S. Army Soldiers of the 289th Military Police Company, 4th Battalion 3rd U.S. Infantry Regiment (The Old Guard), along with their K-9s, conduct training at the Town Hall on Joint Base Myer - Henderson Hall, Va. These K-9s serve multiple functions both here at home and abroad, with numerous deployments in support of Operation Enduring Freedom, where they protected our Soldiers outside the wire.",
            alt: "Army Soldiers along with their K-9's conduct training at the Town Hall on Joint Base Myer - Henderson Hall, Va.",
            homepage: ""
        }]
    }],
    current_year = 2015,
    image_path = "/e2/rv5_images/yearinphotos/" + current_year + "/",
    months="january february march april may june july august september october november december".split(" "),
    current_month="",
    current_photo="";
    $(document).ready(function() {
        $("body").hasClass("index")?makeGallery(): (current_month=$(".month_indicator").attr("id"), current_photo=$(".photo_indicator").attr("id"), makeMonthPage());
        "undefined"!=typeof addthis&&addthis.layers( {
            theme:"transparent", share: {
                position:"left", services:"facebook,twitter,google_plusone_share,pinterest", offset: {
                    top: "230px"
                }
            }
            , thankyou:"false"
        }
        );
        $(window).on("scroll", function() {
            if(768>=$(window).width()) {
                var b=$(window).height();
                $(window).scrollTop()<b&&768>=$(window).width()&& ($("#mobile-nav").hide(), $(".month_title").css("top", "0"), $(".month_title").css("margin-top", "0px"));
                0===$(window).scrollTop()&&768>=$(window).width()&&($(".month_title").css("top", "100px"), $(".month_title").css("margin-top", "9px"), $("#mobile-nav").show())
            }
        }
        )
    }

    );
    function eventTrack() {
        $(".track").on("click", function() {
            var b=$(this).attr("href"), b=b.substr(b.lastIndexOf(".")+1);
            ga("send", "event", "downloads", b, $(this).attr("href"))
        }
        )
    }

    function makeMonthPage() {
        current_photo=parseInt(current_photo);
        var b="",
        d="";
        $.each(photo_data[0], function(c, a) {
            if(c==current_month) {
                current_photo?$(".month_title").html("<h2><a><span id='mobile-prev-btn'  style='float:left'></span></a>"+c.charAt(0).toUpperCase()+c.slice(1)+" - Photo "+current_photo+"<ul id='left-dropdown-menu'class='dropdown-menu pull-left' role='menu'><li><a href='http://www.army.mil/yearinphotos/2015/january.html'>January</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/february.html'>February</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/march.html'>March</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/april.html'>April</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/may.html'>May</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/june.html'>June</a></li></ul><ul id='right-dropdown-menu' class='dropdown-menu pull-right' role='menu'><li><a href='http://www.army.mil/yearinphotos/2015/july.html'>July</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/august.html'>August</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/september.html'>September</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/october.html'>October</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/november.html'>November</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/december.html'>December</a></li></ul></h2>"): $(".month_title").html("<h2><a><span id='mobile-prev-btn' style='float:left'></span></a>"+c.charAt(0).toUpperCase()+c.slice(1)+"<ul id='left-dropdown-menu'class='dropdown-menu pull-left' role='menu'><li><a href='http://www.army.mil/yearinphotos/2015/january.html'>January</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/february.html'>February</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/march.html'>March</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/april.html'>April</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/may.html'>May</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/june.html'>June</a></li></ul><ul id='right-dropdown-menu' class='dropdown-menu pull-right' role='menu'><li><a href='http://www.army.mil/yearinphotos/2015/july.html'>July</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/august.html'>August</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/september.html'>September</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/october.html'>October</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/november.html'>November</a></li><li role='presentation' class='divider'></li><li><a href='http://www.army.mil/yearinphotos/2015/december.html'>December</a></li></ul></h2>");
                $(".month_title").append("<a class='navigation navbar-toggle collapsed' role='navigation' data-toggle='dropdown'><span id='mobile-down-btn' style='float:right'><img class='img-responsive' src='/e2/rv5_images/yearinphotos/2014/dropdown_arrow.png'/></span></a>");
                $("#header_nav ul li").children("#"+c+"").addClass("active");
                $("#footer_nav ul li").children("#"+c+"").addClass("active");
                var d=1;
                $.each(a, function(a, e) {
                    current_photo?current_photo==d&&(b+=makePhoto(c, a, e, current_photo)): b+=makePhoto(c, a, e, d);
                    d++
                }
                )
            }
        }
        );
        $(".dropdown-menu > li > a").each(function() {
            $(this).html().toLowerCase()==current_month&&$(this).parent().addClass("active")
        }
        );
        if(current_photo)1==current_photo?b+=makeNextArrow(current_photo, !1):1<current_photo&&10>current_photo?(b+=makePrevArrow(current_photo, !1), b+=makeNextArrow(current_photo, !1)):b+=makePrevArrow(current_photo, !1);
        else {
            for(var c=0;
            c<months.length;
            c++)current_month==months[c]&&(d=c);
            0===d?b+=makeNextArrow(d, !0): 0<d&&11>d?(b+=makePrevArrow(d, !0), b+=makeNextArrow(d, !0)): b+=makePrevArrow(d, !0)
        }
        $("#month_photos").append(b);
        eventTrack()
    }

    function makeNextArrow(b, d) {
        var c;
        c="<div class='nav_arrow_next'>";
        c=d?c+("<a class='next_month' href='"+months[b+1]+".html' title='Next Month'>Next Month</a>"): c+("<a class='next_month' href='../"+current_month+"/photo_"+parseInt(b+1)+".html' title='Next Photo'>Next Photo</a>");
        return c+"</div>"
    }

    function makePrevArrow(b, d) {
        var c;
        c="<div class='nav_arrow_prev'>";
        c=d?c+("<a class='prev_month' href='"+months[b-1]+".html' title='Previous Month'>Previous Month</a>"): c+("<a class='prev_month' href='../"+current_month+"/photo_"+parseInt(b-1)+".html' title='Previous Photo'>Previous Photo</a>");
        return c+"</div>"
    }

    function makeGallery() {
        activateHovers();
        eventTrack()
    }

    function activateHovers() {
        $(".month_thumb a").hover(function() {
            $(this).find(".rollover").stop().fadeTo(500, 1)
        }
        , function() {
            $(this).find(".rollover").stop().fadeTo(500, 0)
        }
        )
    }

    function makePhoto(b, d, c, f) {
        var a;
        a=""+("<div class='month_photo' id='photo"+(d+1)+"'>")+("<img class='img-responsive'src='"+image_path+"photos/"+b+"/"+c.file_name+".jpg' alt='"+c.alt+"' />");
        a+="<div class='photo_info'>";
        a+="<div class='info_col_left'>";
        a+="<h3>"+c.date+" - "+c.by_line+"</h3>";
        a+="<p id='p-caption'>"+c.caption+"</p>";
        a+='<div id="modal-btn"  data-toggle="modal" data-target="#myModal'+(d+1)+'">View Caption</div>';
        a+='<div class="modal fade" id="myModal'+(d+1)+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
        a+='<div class="modal-dialog">';
        a+='<div class="modal-content">';
        a+='<div class="modal-header">';
        a+='<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        a+='<h4 class="modal-title" id="myModalLabel">'+c.date+" - "+c.by_line+"</h4>";
        a+="</div>";
        a+='<div class="modal-body"><p>'+c.caption+"</p>";
        a+="</div>";
        a+='<div class="modal-footer">';
        a+='<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        a+="</div>";
        a+="</div>";
        a+="</div>";
        a+="</div>";
        a+="</div>";
        a+="<div class='info_col_right'>";
        a+="<a class='track' href='"+image_path+"photos/"+b+"/full/"+c.file_name+".jpg'><span><img src='/e2/rv5_images/yearinphotos/2011/download.png'></span>Download Hi-Res</a>";
        a+="<div class='fb'><iframe src='//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.army.mil%2Fyearinphotos%2F2015%2F"+current_month+"%2Fphoto_"+f+".html&amp;width=124&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=21' scrolling='no' frameborder='0' style='border:none; overflow:hidden; width:124px; height:21px;' allowTransparency='true'></iframe></div>";
        a+="</div>";
        a+="<div class='clearboth'></div>";
        a+="</div>";
        return a+="</div>"
    }

    ;